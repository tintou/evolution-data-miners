/*
 * Copyright 2022 Corentin Noël <corentin.noel@collabora.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include <libedataserver/libedataserver.h>
#include <libebook/libebook.h>
#include <libtracker-sparql/tracker-sparql.h>
#include "missing-autocleanups/missing-autocleanups.h"

#include "edm-contacts-miner.h"
#include "edm-utils.h"

struct _EdmContactsMiner
{
  GObject parent_instance;

  ESourceRegistry *source_registry;
  ESourceRegistryWatcher *watcher;
  GDBusConnection *connection;
  TrackerEndpointDBus *endpoint;
  GCancellable *cancellable;
  GPtrArray *opened_client_views;
};

G_DEFINE_FINAL_TYPE (EdmContactsMiner, edm_contacts_miner, G_TYPE_OBJECT)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

EdmContactsMiner *
edm_contacts_miner_new (void)
{
  return g_object_new (EDM_TYPE_CONTACTS_MINER, NULL);
}

static void
edm_contacts_miner_dispose (GObject *object)
{
  EdmContactsMiner *self = (EdmContactsMiner *)object;

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);
  g_clear_pointer (&self->opened_client_views, g_ptr_array_unref);
  g_clear_object (&self->endpoint);
  g_clear_object (&self->watcher);
  g_clear_object (&self->source_registry);
  g_clear_object (&self->connection);

  G_OBJECT_CLASS (edm_contacts_miner_parent_class)->dispose (object);
}

static void
edm_contacts_miner_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  EdmContactsMiner *self = EDM_CONTACTS_MINER (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
edm_contacts_miner_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  EdmContactsMiner *self = EDM_CONTACTS_MINER (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
edm_contacts_miner_class_init (EdmContactsMinerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = edm_contacts_miner_dispose;
  object_class->get_property = edm_contacts_miner_get_property;
  object_class->set_property = edm_contacts_miner_set_property;
}

static void
edm_create_contact_resource(TrackerResource *addressbook, EContact* contact)
{
  g_autoptr(EContactName) name = NULL;
  g_autofree char *full_name = NULL;
  g_autofree char *uri = NULL;
  TrackerResource *resource;
  const char *uid;
  g_autofree char *affiliation_uri = NULL;
  TrackerResource *affiliation;
  GList *emails;
  GList *tels;

  g_assert(addressbook != NULL);
  g_assert(contact != NULL);

  name = e_contact_get (contact, E_CONTACT_NAME);
  if (!name) {
    g_warning ("No name field!");
    return;
  }

  full_name = e_contact_name_to_string (name);
  uri = tracker_sparql_escape_uri_printf ("urn:contact:%s", full_name);

  resource = tracker_resource_new (uri);
  tracker_resource_set_uri (resource, "rdf:type", "nco:PersonContact");

  uid = e_contact_get_const (contact, E_CONTACT_UID);
  tracker_resource_set_string (resource, "nco:contactUID", uid);

  tracker_resource_set_string (resource, "nco:fullname", full_name);
  if (name->given)
    tracker_resource_set_string (resource, "nco:nameGiven", name->given);
  if (name->family)
    tracker_resource_set_string (resource, "nco:nameFamily", name->family);
  if (name->additional)
    tracker_resource_set_string (resource, "nco:nameAdditional", name->additional);
  if (name->prefixes)
    tracker_resource_set_string (resource, "nco:nameHonorificPrefix", name->prefixes);
  if (name->suffixes)
    tracker_resource_set_string (resource, "nco:nameHonorificSuffix", name->suffixes);

  affiliation_uri = g_strdup_printf ("%s:affiliation", uri);
  affiliation = tracker_resource_new (affiliation_uri);
  tracker_resource_set_uri (affiliation, "rdf:type", "nco:Role");
  emails = e_contact_get (contact, E_CONTACT_EMAIL);
  for (GList *l = emails; l != NULL; l = l->next) {
    g_autofree char *email = g_strdup_printf ("<mailto:%s>", (const char*) l->data);
    tracker_resource_set_string (affiliation, "nco:hasEmailAddress", email);
  }

  g_clear_pointer (&emails, e_contact_attr_list_free);
  tels = e_contact_get (contact, E_CONTACT_TEL);
  for (GList *l = tels; l != NULL; l = l->next) {
    g_autofree char *tel = g_strdup_printf ("<tel:%s>", (const char*) l->data);
    tracker_resource_set_string (affiliation, "nco:hasPhoneNumber", tel);
  }

  g_clear_pointer (&tels, e_contact_attr_list_free);
  tracker_resource_add_take_relation(resource, "nco:hasAffiliation", g_steal_pointer (&affiliation));

  tracker_resource_add_take_relation(addressbook, "nco:containsContact", g_steal_pointer (&resource));
}

static TrackerResource *
edm_create_addressbook_resource(ESource *source)
{
  g_autofree char *uri = tracker_sparql_escape_uri_printf ("urn:addressbook:%s", e_source_get_uid (source));
  TrackerResource *resource = tracker_resource_new (uri);
  tracker_resource_set_uri (resource, "rdf:type", "nco:ContactList");
  tracker_resource_set_string (resource, "nie:title", e_source_get_display_name (source));

  return resource;
}

static void
on_book_objects_added (EdmContactsMiner *self,
                       GSList *contacts,
                       EBookClientView *client_view)
{
  g_autoptr(EBookClient) client = e_book_client_view_ref_client (client_view);
  ESource *source = e_client_get_source (E_CLIENT(client));
  g_autoptr(TrackerResource) addressbook_resource = edm_create_addressbook_resource (source);
  TrackerSparqlConnection *connection;
  g_autoptr(TrackerBatch) batch = NULL;
  g_autoptr(GError) error = NULL;

  for (GSList *l = contacts; l != NULL; l = l->next)
    {
      EContact* contact = l->data;
      edm_create_contact_resource (addressbook_resource, contact);
    }

  connection = tracker_endpoint_get_sparql_connection (TRACKER_ENDPOINT (self->endpoint));
  batch = tracker_sparql_connection_create_batch (connection);
  tracker_batch_add_resource (batch, NULL, addressbook_resource);

  if (!tracker_batch_execute (batch, NULL, &error)) {
    g_printerr ("Couldn't insert batch of resources: %s", error->message);
    return;
  }
}

static void
on_book_objects_removed (G_GNUC_UNUSED EdmContactsMiner *self,
                         GSList *contact_ids,
                         G_GNUC_UNUSED EBookClientView *client_view)
{
  for (GSList *l = contact_ids; l != NULL; l = l->next)
    {
      G_GNUC_UNUSED char* contact_id = l->data;
    }
}

static void
on_book_objects_modified (G_GNUC_UNUSED EdmContactsMiner *self,
                          GSList *contacts,
                          G_GNUC_UNUSED EBookClientView *client_view)
{
  for (GSList *l = contacts; l != NULL; l = l->next)
    {
      G_GNUC_UNUSED EContact* contact = l->data;
    }
}

static void
on_book_client_view (GObject *source_object,
                     GAsyncResult *res,
                     gpointer user_data)
{
  EdmContactsMiner *self = user_data;
  g_autoptr(GError) error = NULL;
  g_autoptr(EBookClientView) view = NULL;

  if (!e_book_client_get_view_finish (E_BOOK_CLIENT (source_object),
                                      res,
                                      &view,
                                      &error))
    {
      g_critical ("Unable to query book client view: %s", error->message);
      return;
    }

  g_signal_connect_object (view, "objects-added", G_CALLBACK (on_book_objects_added), self, G_CONNECT_SWAPPED);
  g_signal_connect_object (view, "objects-removed", G_CALLBACK (on_book_objects_removed), self, G_CONNECT_SWAPPED);
  g_signal_connect_object (view, "objects-modified", G_CALLBACK (on_book_objects_modified), self, G_CONNECT_SWAPPED);

  e_book_client_view_start (view, &error);
  g_ptr_array_add (self->opened_client_views, g_steal_pointer (&view));
}

static void
on_book_client_connected (G_GNUC_UNUSED GObject *source_object,
                          GAsyncResult *res,
                          gpointer user_data)
{
  EdmContactsMiner *self = user_data;
  g_autoptr(EClient) book_client = NULL;
  g_autoptr(GError) error = NULL;

  book_client = e_book_client_connect_finish (res, &error);
  if (!book_client) {
    g_critical ("Unable to open Book client: %s", error->message);
    return;
  }

  e_book_client_get_view (E_BOOK_CLIENT(book_client),
                          "(contains \"x-evolution-any-field\" \"\")",
                          self->cancellable,
                          on_book_client_view,
                          self);
}


gboolean
tracker_dbus_request_name (GDBusConnection  *connection,
                           const gchar      *name,
                           GError          **error)
{
	GError *inner_error = NULL;
	GVariant *reply;
	gint rval;

	reply = g_dbus_connection_call_sync (connection,
	                                     "org.freedesktop.DBus",
	                                     "/org/freedesktop/DBus",
	                                     "org.freedesktop.DBus",
	                                     "RequestName",
	                                     g_variant_new ("(su)",
	                                                    name,
	                                                    0x4 /* DBUS_NAME_FLAG_DO_NOT_QUEUE */),
	                                     G_VARIANT_TYPE ("(u)"),
	                                     0, -1, NULL, &inner_error);
	if (inner_error) {
		g_propagate_prefixed_error (error, inner_error,
		                            "Could not acquire name:'%s'. ",
		                            name);
		return FALSE;
	}

	g_variant_get (reply, "(u)", &rval);
	g_variant_unref (reply);

	if (rval != 1 /* DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER */) {
		g_set_error (error,
		             G_DBUS_ERROR,
		             G_DBUS_ERROR_ADDRESS_IN_USE,
		             "D-Bus service name:'%s' is already taken, "
		             "perhaps the application is already running?",
		             name);
		return FALSE;
	}

	return TRUE;
}

static void
on_address_book_appeared (EdmContactsMiner *self,
				                  ESource *source,
				                  G_GNUC_UNUSED ESourceRegistryWatcher *watcher)
{
  e_book_client_connect (source,
                         -1,
                         self->cancellable,
                         on_book_client_connected,
                         self);
}

static void
on_address_book_disappeared (G_GNUC_UNUSED EdmContactsMiner *self,
				                     G_GNUC_UNUSED ESource *source,
				                     G_GNUC_UNUSED ESourceRegistryWatcher *watcher)
{
}


static void
edm_contacts_miner_init (EdmContactsMiner *self)
{
  g_autoptr(GError) error = NULL;
  g_autoptr(GFile) store = NULL;
  g_autoptr(GFile) ontology = NULL;
  g_autoptr(TrackerSparqlConnection) sparql_conn = NULL;

  self->cancellable = g_cancellable_new ();
  self->opened_client_views = g_ptr_array_new_with_free_func (g_object_unref);
  self->source_registry = e_source_registry_new_sync (self->cancellable, &error);
  if (G_UNLIKELY (!self->source_registry)) {
    g_debug ("Unable to contact Evolution Data Server: %s", error->message);
    return;
  }

  self->connection = g_bus_get_sync (TRACKER_IPC_BUS, self->cancellable, &error);
  if (error) {
    g_critical ("Could not create DBus connection: %s", error->message);
    return;
  }

  store = g_file_new_build_filename (g_get_user_cache_dir (), "tracker3", "eds", NULL);
  ontology = tracker_sparql_get_ontology_nepomuk ();
  sparql_conn = tracker_sparql_connection_new (TRACKER_SPARQL_CONNECTION_FLAGS_NONE,
                                               store,
                                               ontology,
                                               self->cancellable,
                                               &error);
  if (!sparql_conn) {
    g_critical ("Unable to create SPARQL connection: %s", error->message);
    return;
  }

  self->endpoint = tracker_endpoint_dbus_new (sparql_conn,
                                              self->connection,
                                              NULL,
                                              self->cancellable,
                                              &error);
  if (!self->endpoint) {
    g_critical ("Unable to create endpoint: %s", error->message);
    return;
  }

  if (!tracker_dbus_request_name (self->connection, "org.freedesktop.Tracker3.Miner.EDS", &error)) {
    g_critical ("Unable to own name: %s", error->message);
    return;
  }

  self->watcher = e_source_registry_watcher_new (self->source_registry, E_SOURCE_EXTENSION_ADDRESS_BOOK);

	g_signal_connect_object (self->watcher, "appeared", G_CALLBACK (on_address_book_appeared), self, G_CONNECT_SWAPPED);
	g_signal_connect_object (self->watcher, "disappeared", G_CALLBACK (on_address_book_disappeared), self, G_CONNECT_SWAPPED);
  e_source_registry_watcher_reclaim (self->watcher);
}

